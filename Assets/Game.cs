﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Game : MonoBehaviour {

    public GameObject title, gameover,play,exit;
    public GameObject Plane,Enemy;
    public float time;

    public bool playing = false;
    
    public Text ScoreText;
    public int score;

    public static Game obj;

	// Use this for initialization
	void Start () {
        obj = this;
        gameover.SetActive(false);
	}

    public void startGame(){
        playing = true;
        title.SetActive(false);
        play.SetActive(false);
        exit.SetActive(false);
        gameover.SetActive(false);
        var pos = new Vector3(0, -3, 0);
        Instantiate(Plane, pos, transform.rotation);
        resetScore();
    }

    public void gameOver()
    {
        playing = false;
        gameover.SetActive(true);
        play.SetActive(true);
        exit.SetActive(true);
        Array.ForEach(GameObject.FindGameObjectsWithTag("Enemy"), e => Destroy(e));
    }
    public void exitGame(){
        Application.Quit();
    }

    // Update is called once per frame
    void Update () {
        time += Time.deltaTime;
        if(time>0.5 && playing)
        {
            var pos = new Vector3(Random.Range(-2.5f, 2.5f), 3, 0);
            Instantiate(Enemy, pos, transform.rotation);
            time = 0;
        }
	}

    public void resetScore(){
        score = 0;
        ScoreText.text = "Score: " + score;
    }
    public void addScore(){
        score++;
        ScoreText.text = "Score: " + score;
    }
}
