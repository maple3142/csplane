﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveEnemy : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.tag=="Enemy"){
            Destroy(col.gameObject);
        }
    }
}
